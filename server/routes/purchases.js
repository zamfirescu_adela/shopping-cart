const express = require("express");
const router = express.Router();

const { purchases } = require("../controllers");

router.get("/cart", purchases.getShoppingCart);
router.put("/cart", purchases.updateShoppingCart);

router.post("/", purchases.executePurchase);

router.get("/", purchases.getPurchasesPerUser);
router.get("/all", purchases.getAllPurchases);
router.put("/", purchases.editPurchase);

module.exports = router;
