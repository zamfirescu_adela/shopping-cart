const express = require("express");
const router = express.Router();

const { isAuthenticated } = require("../controllers").auth.middleware;

const reset = require("./reset");
const auth = require("./auth");
const preferences = require("./preferences");
const categories = require("./categories");
const products = require("./products");
const purchases = require("./purchases");
const discount = require("./discount");

router.use("/reset", reset);
router.use("/auth", auth);
router.use("/preferences", preferences);
router.use("/products", products);
router.use("/categories", categories);
router.use("/purchases", isAuthenticated, purchases);
router.use("/discount", isAuthenticated, discount);

module.exports = router;
