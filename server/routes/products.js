const express = require("express");
const router = express.Router();

const { products } = require("../controllers");

router.get("/", products.getProducts);

router.post("/", products.addProduct);

router.delete("/", products.removeProduct);

router.put("/", products.editProduct);

module.exports = router;
