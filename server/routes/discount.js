const express = require("express");
const router = express.Router();

const { discount } = require("../controllers");

router.get("/voucher", discount.getVoucherValue);
router.post("/voucher", discount.addVoucher);
router.get("/vouchers", discount.getAllVouchers);

module.exports = router;
