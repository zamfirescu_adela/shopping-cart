const { Preference } = require("../models");

const controller = {
  editPreferences: async (req, res) => {
    try {
      const { name, value } = req.body;
      const preference = await Preference.findOne({
        attributes: ["id", "name"],
        where: { name },
      });

      if (!preference) {
        await Preference.create({ name, value });
      } else {
        await preference.update({ ...preference, value });
      }

      res.status(200).send({ message: `Preference ${name} was saved` });
    } catch (err) {
      res.status(500).send({ message: err });
    }
  },
  getPreferences: async (req, res) => {
    try {
      const preferences = await Preference.findAll({
        attributes: ["name", "value"],
        raw: true,
      });

      res.status(200).send(preferences);
    } catch (err) {
      res.status(500).send({ message: err });
    }
  },
};

module.exports = controller;
