const { Category } = require("../models");

const controller = {
  getCategories: async (req, res) => {
    try {
      const categories = await Category.findAll({
        attributes: ["id", "name"],
        raw: true,
      });

      res.status(200).send(categories);
    } catch (err) {
      res.status(500).send({ message: err });
    }
  },
  addCategory: async (req, res) => {
    try {
      const { name } = req.body;

      await Category.create({
        name,
      });

      res.status(200).send({ message: `Category ${name} was added.` });
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: err });
    }
  },
  removeCategory: async (req, res) => {
    try {
      const { id } = req.body;

      const category = await Category.findOne({
        where: {
          id,
        },
      });

      if (!category) {
        res.status(404).send({ message: `Category doesn't exists` });
      } else {
        await category.destroy();

        res
          .status(200)
          .send({ message: `Category ${category.name} was deleted.` });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: err });
    }
  },
};

module.exports = controller;
