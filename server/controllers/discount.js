const { Op } = require("sequelize");
const { Voucher } = require("../models");

const controller = {
  getVoucherValue: async (req, res) => {
    try {
      const { code } = req.query;

      const voucher = await Voucher.findOne({
        where: {
          code,
          limit: {
            [Op.gt]: 0,
          },
        },
        raw: true,
      });

      if (voucher) {
        res.status(200).send({ value: voucher.value, type: voucher.type });
      } else {
        res.status(404).send({ message: "Invalid voucher" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  addVoucher: async (req, res) => {
    try {
      const { value, limit, type } = req.body;

      let exists = true;
      let code;
      while (exists) {
        code = Math.random()
          .toString(36)
          .replace(/[^a-z]+/g, "")
          .substr(0, 5)
          .toLocaleUpperCase();

        const voucher = await Voucher.findOne({
          where: {
            code,
          },
          raw: true,
        });
        if (!voucher) exists = false;
      }

      const voucher = await Voucher.create({ code, type, limit, value });
      res.status(201).send(voucher);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  getAllVouchers: async (req, res) => {
    try {
      const vouchers = await Voucher.findAll({
        attributes: ["id", "code", "type", "value", "limit"],
        raw: true,
      });

      res.status(200).send(vouchers);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
};

module.exports = controller;
