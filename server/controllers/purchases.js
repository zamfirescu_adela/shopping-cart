const { Op } = require("sequelize");
const {
  Product,
  Preference,
  Category,
  Purchase,
  ProductPreference,
  ShoppingCart,
  Voucher,
  ProductPurchase,
  User,
} = require("../models");

const controller = {
  getShoppingCart: async (req, res) => {
    try {
      const products = await ShoppingCart.findAll({
        attributes: ["id", "quantity", "userId", "productPreferenceId"],
        where: { userId: req.user.id },
        raw: true,
      });

      const productsFormatted = await Promise.all(
        products.map(async (product) => {
          const preference = await ProductPreference.findOne({
            attributes: [
              "id",
              "size",
              "color",
              "productId",
              "quantityAvailable",
            ],
            where: { id: product.productPreferenceId },
            raw: true,
          });
          const detail = await Product.findOne({
            attributes: [
              "id",
              "name",
              "price",
              "description",
              "categoryId",
              "image",
            ],
            where: { id: preference.productId },
            raw: true,
          });

          const category = await Category.findOne({
            attributes: ["id", "name"],
            where: { id: detail.categoryId },
            raw: true,
          });

          return { ...product, category, preference, detail };
        })
      );

      res.status(200).send(productsFormatted);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  updateShoppingCart: async (req, res) => {
    try {
      const { items } = req.body;

      let valid = true;
      let found = true;

      const detailedItems = await Promise.all(
        items.map(async (item) => {
          const preference = await ProductPreference.findOne({
            attributes: [
              "id",
              "size",
              "color",
              "quantityAvailable",
              "productId",
            ],
            where: { id: item.id },
            raw: true,
          });

          if (!preference) {
            found = false;
            return null;
          } else if (preference.quantityAvailable < item.quantity) {
            valid = false;
          }
          return { ...item, preference };
        })
      );
      if (found) {
        if (valid) {
          const dbItems = await ShoppingCart.findAll({
            attributes: ["id", "quantity", "userId", "productPreferenceId"],
            where: { userId: req.user.id },
          });

          const toBeRemoved = dbItems.filter(
            (item) =>
              !detailedItems.find((det) => det.id == item.productPreferenceId)
          );

          await Promise.all(
            detailedItems.map(async (item) => {
              const savedItem = await ShoppingCart.findOne({
                where: { userId: req.user.id, productPreferenceId: item.id },
              });

              if (!savedItem) {
                await ShoppingCart.create({
                  quantity: item.quantity,
                  productPreferenceId: item.id,
                  userId: req.user.id,
                });
              } else {
                await savedItem.update({
                  ...savedItem,
                  quantity: item.quantity ? item.quantity : savedItem.quantity,
                });
              }
            })
          );

          await Promise.all(
            toBeRemoved.map(async (item) => {
              let savedItem;
              if (item.id) {
                savedItem = await ShoppingCart.findOne({
                  where: { id: item.id },
                });
              }

              if (savedItem) {
                await savedItem.destroy();
              }
            })
          );

          res.status(200).send({ message: "Shopping cart updated" });
        } else {
          res.status(400).send({ message: "Quantity too much" });
        }
      } else {
        res.status(404).send({ message: "Product not found" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },

  // getVoucherValue: async (req, res) => {
  //   try {
  //     const { code } = req.query;

  //     const voucher = await Voucher.findOne({
  //       where: {
  //         code,
  //         limit: {
  //           [Op.gt]: 0,
  //         },
  //       },
  //       raw: true,
  //     });

  //     if (voucher) {
  //       res.status(200).send({ value: voucher.value });
  //     } else {
  //       res.status(404).send({ message: "Invalid voucher" });
  //     }
  //   } catch (err) {
  //     console.error(err);
  //     res.status(500).send({ message: "Server error" });
  //   }
  // },

  executePurchase: async (req, res) => {
    try {
      const { code, onlinePayment } = req.body;

      const voucher = code
        ? await Voucher.findOne({
            where: {
              code,
              limit: {
                [Op.gt]: 0,
              },
            },
            raw: true,
          })
        : null;

      if ((code && voucher) || (!code && !voucher)) {
        const products = await ShoppingCart.findAll({
          where: { userId: req.user.id },
        });

        let valid = true;
        let found = true;

        const preferences = await Promise.all(
          products.map(async (item) => {
            const preference = await ProductPreference.findOne({
              attributes: [
                "id",
                "size",
                "color",
                "quantityAvailable",
                "productId",
              ],
              where: { id: item.productPreferenceId },
            });

            if (!preference) {
              found = false;
              return null;
            } else if (preference.quantityAvailable < item.quantity) {
              valid = false;
            }
            return preference;
          })
        );
        if (products.length !== 0) {
          if (found) {
            if (valid) {
              const today = new Date();

              const purchase = await Purchase.create({
                status: onlinePayment ? "PAID" : "RECEIVED",
                isPaid: onlinePayment ? true : false,
                userId: req.user.id,
                deliveryDate: new Date(
                  today.setDate(today.getDate() + 7)
                ).toLocaleDateString("en-US"),
                voucherId: voucher ? voucher.id : null,
              });

              await Promise.all(
                products.map(async (product) => {
                  await ProductPurchase.create({
                    quantity: product.quantity,
                    productPreferenceId: product.productPreferenceId,
                    purchaseId: purchase.id,
                  });

                  const prefToChange = preferences.find(
                    (pref) => pref.id === product.productPreferenceId
                  );

                  await prefToChange.update({
                    ...prefToChange,
                    quantityAvailable:
                      prefToChange.quantityAvailable - product.quantity,
                  });
                })
              );

              await Promise.all(
                products.map(async (product) => {
                  await product.destroy();
                })
              );

              res.status(200).send({ message: "Purchase executed" });
            } else {
              res
                .status(400)
                .send({ message: "Quantity too high for one of the items" });
            }
          } else {
            res
              .status(404)
              .send({ message: "One of the items doesn't exists anymore" });
          }
        } else res.status(400).send({ message: "No items in shopping cart" });
      } else if (code && !voucher) {
        res.status(404).send({ message: "Invalid voucher" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  getPurchasesPerUser: async (req, res) => {
    try {
      const purchases = await Purchase.findAll({
        where: { userId: req.user.id },
        raw: true,
      });

      const formattedPurchases = await Promise.all(
        purchases.map(async (purchase) => {
          const items = await ProductPurchase.findAll({
            where: { purchaseId: purchase.id },
            raw: true,
          });
          const voucher = purchase.voucherId
            ? await Voucher.findOne({
                attributes: ["type", "value"],
                where: { id: purchase.voucherId },
                raw: true,
              })
            : null;

          let total = 0;
          const formattedItems = await Promise.all(
            items.map(async (item) => {
              const preference = await ProductPreference.findOne({
                attributes: ["id", "size", "color", "productId"],
                where: { id: item.productPreferenceId },
                raw: true,
              });

              const detail = await Product.findOne({
                attributes: [
                  "id",
                  "name",
                  "price",
                  "description",
                  "categoryId",
                ],
                where: { id: preference.productId },
                raw: true,
              });
              total = total + detail.price * item.quantity;
              const category = await Category.findOne({
                attributes: ["id", "name"],
                where: { id: detail.categoryId },
                raw: true,
              });

              return { ...item, preference, detail, category };
            })
          );

          if (voucher)
            if (voucher.type == "percent")
              total = total - total * (voucher.value / 100);
            else total = total - voucher.value;
          total = total > 0 ? total + 20 : 20;
          return { ...purchase, items: formattedItems, voucher, total };
        })
      );
      res.status(200).send(formattedPurchases);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
  getAllPurchases: async (req, res) => {
    try {
      const purchases = await Purchase.findAll({
        raw: true,
      });

      const formattedPurchases = await Promise.all(
        purchases.map(async (purchase) => {
          const items = await ProductPurchase.findAll({
            where: { purchaseId: purchase.id },
            raw: true,
          });

          const user = await User.findOne({
            attributes: [
              "id",
              "firstName",
              "lastName",
              "email",
              "phone",
              "address",
              "city",
              "state",
              "zip",
            ],
            where: { id: purchase.userId },
            raw: true,
          });
          let total = 0;
          const formattedItems = await Promise.all(
            items.map(async (item) => {
              const preference = await ProductPreference.findOne({
                attributes: ["id", "size", "color", "productId"],
                where: { id: item.productPreferenceId },
                raw: true,
              });
              const detail = await Product.findOne({
                attributes: [
                  "id",
                  "name",
                  "price",
                  "description",
                  "categoryId",
                ],
                where: { id: preference.productId },
                raw: true,
              });
              total = total + detail.price * item.quantity;
              const category = await Category.findOne({
                attributes: ["id", "name"],
                where: { id: detail.categoryId },
                raw: true,
              });

              return { ...item, preference, detail, category };
            })
          );
          const voucher = purchase.voucherId
            ? await Voucher.findOne({
                attributes: ["type", "value"],
                where: { id: purchase.voucherId },
                raw: true,
              })
            : null;

          if (voucher)
            if (voucher.type == "percent")
              total = total - total * (voucher.value / 100);
            else total = total - voucher.value;

          total = total > 0 ? total + 20 : 20;
          return { ...purchase, user, items: formattedItems, voucher, total };
        })
      );
      res.status(200).send(formattedPurchases);
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },

  editPurchase: async (req, res) => {
    try {
      const { id, status } = req.body;
      const purchase = await Purchase.findOne({
        where: { id },
      });

      if (!status) {
        res.status(400).send({ message: "Status invalid" });
      } else if (!purchase)
        res.status(404).send({ message: "Purchase not found" });
      else {
        await purchase.update({
          ...purchase,
          status,
        });
        res.status(200).send({ message: "Status actualizat" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).send({ message: "Server error" });
    }
  },
};

module.exports = controller;
