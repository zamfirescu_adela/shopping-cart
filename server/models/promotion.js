module.exports = function (sequelize, DataTypes) {
  return sequelize.define("promotion", {
    startDate: DataTypes.STRING,
    endDate: DataTypes.STRING,
    discountValue: DataTypes.FLOAT,
  });
};
