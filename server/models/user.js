module.exports = function (sequelize, DataTypes) {
  return sequelize.define("user", {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN,
    phone: DataTypes.STRING,
    address: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    zip: DataTypes.STRING,
    nameOnCard: DataTypes.STRING,
    cardNumber: DataTypes.STRING,
    cvv: DataTypes.STRING,
    expDate: DataTypes.STRING,
  });
};
