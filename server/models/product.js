module.exports = function (sequelize, DataTypes) {
  return sequelize.define("products", {
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
    description: DataTypes.STRING,
    image: DataTypes.TEXT,
  });
};
