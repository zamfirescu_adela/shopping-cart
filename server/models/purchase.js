module.exports = function (sequelize, DataTypes) {
  return sequelize.define("purchase", {
    status: DataTypes.STRING,
    deliveryDate: DataTypes.STRING,
    isPaid: DataTypes.BOOLEAN,
  });
};
