module.exports = function (sequelize, DataTypes) {
  return sequelize.define("preference", {
    name: DataTypes.STRING,
    value: DataTypes.TEXT,
  });
};
