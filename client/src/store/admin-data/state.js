export default {
  brandName: "YouFashion",
  noItems: 6,
  voucher: null,
  onlinePayment: true,
  adminPreferences: [],
  currentUserProducts: [],
  allUsersProducts: [],
  categories: [],
  products: [],
  promotions: [],
  shoppingCart: [],
  guestShoppingCart: [],
  vouchers: [],
  colors: [
    {
      id: 0,
      name: "blue"
    },
    {
      id: 1,
      name: "red"
    }
  ],
  sizes: [
    {
      id: 0,
      name: "XXS"
    },
    {
      id: 1,
      name: "XS"
    },
    {
      id: 2,
      name: "S"
    },
    {
      id: 3,
      name: "M"
    },
    {
      id: 4,
      name: "L"
    },
    {
      id: 5,
      name: "XL"
    },
    {
      id: 6,
      name: "XXL"
    }
  ]
};
